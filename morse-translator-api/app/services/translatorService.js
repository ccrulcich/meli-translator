let	chars = {
		A: ".-", 
		B: "-...", 
		C: "-.-.", 
		D: "-..", 
		E: ".", 
		F: "..-.", 
		G: "--.", 
		H: "....", 
		I: "..", 
		J: ".---", 
		K: "-.-", 
		L: ".-..", 
		M: "--", 
		N: "-.", 
		O: "---", 
		P: ".--.", 
		Q: "--.-", 
		R: ".-.", 
		S: "...", 
		T: "-", 
		U: "..-", 
		V: "...-", 
		W: ".--", 
		X: "-..-", 
		Y: "-.--", 
		Z: "--..",
		" ": "",
		"1": ".----",
		"2": "..---",
		"3": "...--",
		"4": "....-",
		"5": ".....",
		"6": "-....",
		"7": "--...",
		"8": "---..",
		"9": "----.",
		"0": "-----",
		"": ".-.-.-",
	}

let translateToHuman = function (value) {
	for (var key in chars) { 
        if (chars.hasOwnProperty(key)) { 
            if (chars[key] === value) 
            return key; 
        } 
	}
	throw new Error("Unrecognized human character for " + value);
}

let translateToMorse = function (value) {
	if (!chars.hasOwnProperty(value)) { 
		throw new Error("Unrecognized morse code character for " + value);
	}
	return chars[value];
}
				
module.exports = {
    translateToHuman: translateToHuman,
	translateToMorse: translateToMorse
};