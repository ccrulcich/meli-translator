let assistantsService = require('../services/translatorService');

module.exports = function(app, db) {
	
	app.post('/2morse', (req, res) => {
		try {
			let morseArray = [];
			if (req.body.text) {
				let textArray = req.body.text.split('');
				for( var i = 0; i < textArray.length; i++) {
					morseArray.push(assistantsService.translateToMorse(textArray[i]));
				}
			}
			
			res.header('Access-Control-Allow-Origin', '*');
			res.send(JSON.parse('{ "translateText": "' + morseArray.join(' ') + '"} '));
		}
		catch (error) {
			res.header('Access-Control-Allow-Origin', '*');
			res.status(500).json({ 'result': error.message});
		}

	});
	
	app.post('/2text', (req, res) => {
		try {
			let textArray = [];
			if (req.body.text) {
				let morseArray = req.body.text.split(' ');
				for( var i = 0; i < morseArray.length; i++) { 
					if ( morseArray[i] === ' ') { 
						morseArray.splice(i, 1); 
					}
					else {
						textArray.push(assistantsService.translateToHuman(morseArray[i]));
					}
				}
				
			}
			
			res.header('Access-Control-Allow-Origin', '*');
			res.send(JSON.parse('{ "translateText": "' + textArray.join('') + '"} '));
		}
		catch (error) {
			res.header('Access-Control-Allow-Origin', '*');
			res.status(500).json({ 'result': error.message});
		}		
		

	});
	
};