const morseTranslatorRoutes = require('./morseTranslator_routes');

module.exports = function(app, db) {
  morseTranslatorRoutes(app, db);
};