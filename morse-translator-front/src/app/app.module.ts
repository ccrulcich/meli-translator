import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TranslatorService } from './translator/translator.service';
import { TranslatorComponent } from './translator/translator.component';

import { ConfigurationService } from './config/config.service';

@NgModule({
  declarations: [
    AppComponent,
	TranslatorComponent	
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	HttpClientModule,
	ReactiveFormsModule
  ],
  providers: [
	TranslatorService,
	ConfigurationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
