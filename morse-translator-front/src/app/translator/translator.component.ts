import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { TranslatorService } from './translator.service';

@Component({
    selector: 'translator',
    templateUrl: './translator.component.html'
})

export class TranslatorComponent implements OnInit {

	public translatorForm: FormGroup = new FormGroup({
		bitText: new FormControl(''),
		bitAuxText : new FormControl(''),
		morseText: new FormControl(''),
		humanText: new FormControl('')
	});
	
	private interval: any;
	
	public invalidHumanControlText: string;
	public invalidMorseControlText: string;
	public invalidBITControlText: string;
	public runningBITTranslate: boolean = false;
	
	constructor(private translatorService: TranslatorService) { 
	}

	ngOnInit() {
		this.translatorForm.controls['humanText'].statusChanges.subscribe(status => {
			this.invalidHumanControlText = status === "VALID" ? "" : this.invalidHumanControlText;
		});
		
		this.translatorForm.controls['morseText'].statusChanges.subscribe(status => {
			this.invalidMorseControlText = status === "VALID" ? "" : this.invalidMorseControlText;
		});
		
		this.translatorForm.controls['bitText'].statusChanges.subscribe(status => {
			if (status === "VALID") {
				this.invalidBITControlText = "";
			} else {
				this.invalidBITControlText = "Invalid BIT string. Please restart the translation.";
				this.stopTranslateBitToMorse();
			}
		});
	}

	startTranslateBitToMorse() :void {
		let translatorComponent: TranslatorComponent = this;
		let counterWithoutAction: number = 0;
		let bitControl = this.translatorForm.controls['bitText'];
		let bitAuxControl  = this.translatorForm.controls['bitAuxText'];
		let morseControl = this.translatorForm.controls['morseText'];
		
		morseControl.setValue('');
		bitAuxControl.setValue('');
		bitControl.setValue('');
		
		this.interval = setInterval(function(){ 
			translatorComponent.runningBITTranslate = true;
			
			if (counterWithoutAction > 10 || morseControl.value.includes('.-.-.-')) {
				translatorComponent.stopTranslateBitToMorse();
				return;
			}
			
			if (bitControl.value === "") {
				morseControl.setValue('');
				return;
			}
			
			let lastChar = bitControl.value.substr(bitControl.value.length - 1);

			if (bitAuxControl.value !== bitControl.value) {
				bitAuxControl.setValue(bitControl.value);
				counterWithoutAction = 0;
				if (lastChar === '0') {
					morseControl.setValue(morseControl.value + '.');
				}
				if (lastChar === '1') {
					morseControl.setValue(morseControl.value + '-');
				}
			}
			else {
				morseControl.setValue(morseControl.value + ' ');
				counterWithoutAction += 1;
			}
			
		}, 1000);
	}
	
	stopTranslateBitToMorse() :void {
		clearInterval(this.interval);
		this.runningBITTranslate = false;
	}
	
	translateToHuman() : void {
		this.translatorService.translateToHuman(this.translatorForm.controls['morseText'].value).subscribe(
			(humanText : any) => {
				this.translatorForm.controls['humanText'].setValue(humanText.translateText);
			},
			error => {
				this.translatorForm.controls['morseText'].setErrors({'invalid': error.error.result});
				this.invalidMorseControlText = error.error.result;
			}
		);
	}
  
	translateToMorse() : void {
		this.translatorService.translateToMorse(this.translatorForm.controls['humanText'].value).subscribe(
			(morseText : any) => {
				this.translatorForm.controls['morseText'].setValue(morseText.translateText);
			},
			error => {
				this.translatorForm.controls['humanText'].setErrors({'invalid': error.error.result});
				this.invalidHumanControlText = error.error.result;
			}
		);
	}
}