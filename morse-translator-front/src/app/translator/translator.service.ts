import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

import { ConfigurationService } from '../config/config.service';

@Injectable()
export class TranslatorService {

	constructor(private http: HttpClient, private configurationService: ConfigurationService) { 
	}

	translateToHuman(morseText: string) : Observable<string> {
	    const apiUrl = `${this.configurationService.getAPIConfig()}/2text`;
		return this.http.post<string>(apiUrl, { 'text': morseText });
	}
  
	translateToMorse(humanText: string) : Observable<string> {
		const apiUrl = `${this.configurationService.getAPIConfig()}/2morse`;
		return this.http.post<string>(apiUrl, { 'text': humanText });
	}
}