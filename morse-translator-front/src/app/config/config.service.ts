import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ConfigurationService {
	private APIUrl: string = 'https://meli-translator.appspot.com';
	//private APIUrl: string = 'http://localhost:8080';

	getAPIConfig() {
		return this.APIUrl;
	}
}